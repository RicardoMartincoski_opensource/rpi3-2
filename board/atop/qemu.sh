#!/bin/sh
set -e -x

BASE_DIR="$(dirname "$0")/../.."
"${BASE_DIR}/rpi3/board/rpi3_64_linux_sysv/qemu.sh" atop
