[TOC]

# rpi3-2

------

## Description

Example 2nd layer br2-external overlay to generate images for Raspberry Pi 3.

------

## License

SPDX-License-Identifier: GPL-2.0-only

    rpi3-2
    Copyright (C) 2022  Ricardo Martincoski  <ricardo.martincoski@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

------

There are some files in the tree copied from other projects.
All of such files were originally licensed using a license that allows them to
be redistributed following GPL-2.0.
They have the license stated in the header of the file. For instance:

    SPDX-License-Identifier: GPL-2.0-or-later
    copied from <Project name> <version>
    and then updated:
    - to do <something nice>

------

## How to run the tests

Assumption: using a computer with `Ubuntu 20.04.2.0`.

### Install `docker` and add user to group

```
$ sudo apt install docker.io
$ sudo groupadd docker
$ sudo usermod -aG docker $USER # NOTE: logout/login after this
$ docker run hello-world
```

### Install `git` and `make`

```
$ sudo apt install git make
```

### Download the repo

```
$ git clone --depth 1 --recurse-submodules \
  https://gitlab.com/RicardoMartincoski_opensource/rpi3-2.git
$ cd rpi3-2
```

### Generate the images

```
$ make
```

### Test the image using qemu

```
$ board/atop/qemu.sh
```
User `root` password `root`.

**WARNING**
    This example uses ultra-INSECURE ssh configurations, do not use for real
    projects

Ctrl+A,C opens the console in which one can use `quit` to abruptly stop qemu.
```
(qemu) quit
```

### Burn the image

Follow rpi3/buildroot/board/raspberrypi3-64/readme.txt
```
$ sudo dd if=output/atop/images/sdcard.img
```
**WARNING**
    This command can take few minutes to run, depending on the size of the image
    and all speed limitations between the CPU of your computer and the SD card.
    For instance the USB revision or the SD manufacturer.

#### When your have partition auto-mounter enabled on your distro

Manually unmount any previous partition and also write the whole image as a
single block (just use bs=SIZE larger than the .img file) in order to prevent
auto-mount between two blocks are written to the SD card.
```
$ mount | grep sdX # check there are mounted partitions
$ sudo umount /dev/sdX*
$ mount | grep sdX # check there is no mounted partitions
$ sudo dd if=output/atop/images/sdcard.img of=/dev/sdX bs=2G count=1
```
Wait the auto-mounter to mount the new partitions.
Manually unmount the new partitions before removing the SD card.

### Boot your image

Plug into your Raspberry Pi 3:
- the SD card (at least 512MiB);
- an HDMI display;
- a USB keyboard;
- an Ethernet cable;

### Connect to the target

At the HDMI console, using the keyboard attached to it, check the IP it got
using DHCP:
```
$ ip addr # check TARGET_IP shown at eth0
```
From your host PC, connect to the target IP:
```
$ sshpass -v -p root ssh root@TARGET_IP
```
**WARNING**
    This example uses ultra-INSECURE ssh configurations, do not use for real
    projects

### Update your image

At your host (the PC building the image) copy the .swu file to a webserver
(apache2 is an easy one to setup on Ubuntu machines)
```
$ cp output/atop/images/rootfs.swu /HOST/PATH/TO/HTTP/SERVER/
```
At your target (either via HDMI console or vis ssh):
```
# /opt/update http://HOST_IP_HTTP_SERVER/rootfs.swu
```
