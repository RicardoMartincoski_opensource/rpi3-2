BR2_EXTERNAL2_DIR := $(shell readlink -f .)
BR2_EXTERNAL1_DIR := $(BR2_EXTERNAL2_DIR)/rpi3
BUILDROOT_DIR := $(BR2_EXTERNAL1_DIR)/buildroot
BUILD_OUTPUT_DIR := $(BR2_EXTERNAL2_DIR)/output
BUILD_SRC_DIR := $(BR2_EXTERNAL2_DIR)/config-fragments
DEFCONFIG_SUFFIX := _defconfig.fragment
CCACHE_DIR := $(BR2_EXTERNAL2_DIR)/.ccache
DOWNLOAD_CACHE_DIR := $(BR2_EXTERNAL2_DIR)/download
BRMAKE_SCRIPT := $(BUILDROOT_DIR)/utils/brmake
MERGE_CONFIG_SCRIPT := $(BUILDROOT_DIR)/support/kconfig/merge_config.sh
DIFF_CONFIG_SCRIPT := $(BUILDROOT_DIR)/utils/diffconfig

check_inside_docker := $(shell if [ "`groups`" = 'br-user' ]; then echo y; else echo n; fi)
date := $(shell date +%Y%m%d.%H%M --utc)
configs_to_build := $(notdir $(patsubst %$(DEFCONFIG_SUFFIX),%,$(wildcard $(BUILD_SRC_DIR)/*)))

real_targets_inside_docker := \
	.stamp_config $(foreach defconfig,$(configs_to_build),.stamp_config_$(defconfig)) \
	.stamp_source $(foreach defconfig,$(configs_to_build),.stamp_source_$(defconfig)) \
	.stamp_toolchain $(foreach defconfig,$(configs_to_build),.stamp_toolchain_$(defconfig)) \
	.stamp_os_depends $(foreach defconfig,$(configs_to_build),.stamp_os_depends_$(defconfig)) \
	.stamp_os $(foreach defconfig,$(configs_to_build),.stamp_os_$(defconfig)) \
	.stamp_init $(foreach defconfig,$(configs_to_build),.stamp_init_$(defconfig)) \
	.stamp_updater $(foreach defconfig,$(configs_to_build),.stamp_updater_$(defconfig)) \
	.stamp_all $(foreach defconfig,$(configs_to_build),.stamp_all_$(defconfig)) \

real_targets_outside_docker := \
	.stamp_submodules \

phony_targets_inside_docker := \
	config $(foreach defconfig,$(configs_to_build),$(defconfig)+config) \
	source $(foreach defconfig,$(configs_to_build),$(defconfig)+source) \
	toolchain $(foreach defconfig,$(configs_to_build),$(defconfig)+toolchain) \
	os-depends $(foreach defconfig,$(configs_to_build),$(defconfig)+os-depends) \
	os $(foreach defconfig,$(configs_to_build),$(defconfig)+os) \
	init $(foreach defconfig,$(configs_to_build),$(defconfig)+init) \
	updater $(foreach defconfig,$(configs_to_build),$(defconfig)+updater) \
	all $(foreach defconfig,$(configs_to_build),$(defconfig)+all) \
	menuconfig $(foreach defconfig,$(configs_to_build),$(defconfig)+menuconfig) \
	legal-info $(foreach defconfig,$(configs_to_build),$(defconfig)+legal-info) \
	check-package \

phony_targets_outside_docker := \
	$(foreach defconfig,$(configs_to_build),$(defconfig)+) \
	submodules \
	clean-stamps $(foreach defconfig,$(configs_to_build),$(defconfig)+clean-stamps) \
	clean-target-workaround-failed-reinstall $(foreach defconfig,$(configs_to_build),$(defconfig)+clean-target-workaround-failed-reinstall) \
	clean-target $(foreach defconfig,$(configs_to_build),$(defconfig)+clean-target) \
	reconfig $(foreach defconfig,$(configs_to_build),$(defconfig)+reconfig) \
	rebuild $(foreach defconfig,$(configs_to_build),$(defconfig)+rebuild) \
	image $(foreach defconfig,$(configs_to_build),$(defconfig)+image) \
	clean $(foreach defconfig,$(configs_to_build),$(defconfig)+clean) \
	distclean \
	help \

.PHONY: default $(phony_targets_inside_docker) $(phony_targets_outside_docker)
default: .stamp_all

help:
	@echo "rpi3-2 version $$(git describe --always), Copyright (C) 2022  Ricardo Martincoski"
	@echo "  rpi3-2 comes with ABSOLUTELY NO WARRANTY; for details see file LICENSE."
	@echo "  SPDX-License-Identifier: GPL-2.0-only"
	@echo
	@echo 'These targets build a <defconfig>:'
	@$(foreach defconfig,$(configs_to_build), \
		echo '   $(defconfig)+'; \
	)
	@echo
	@echo 'Dependency chain:'
	@echo ' submodules -> config -> source -> toolchain -> os-depends -> os -> init -> updater -> all'
	@echo
	@echo 'These targets invoke docker when called outside it:'
	@echo ' * config                       = configure the build for all defconfigs'
	@echo ' * source                       = "make source" for all defconfigs'
	@echo ' * toolchain                    = "make toolchain" for all defconfigs'
	@echo ' * os-depends                   = "make linux-depends" or "make uboot-depends" for all defconfigs'
	@echo ' * os                           = "make linux" or "make uboot" for all defconfigs'
	@echo ' * init                         = "make systemd" for all defconfigs'
	@echo ' * updater                      = "make swupdate" for all defconfigs'
	@echo ' * all                (default) = "make all" for all defconfigs'
	@echo ' * menuconfig                   = "make menuconfig" for all defconfigs'
	@echo ' * legal-info                   = "make legal-info" for all defconfigs'
	@echo '   check-package                = run check-package for all files in the br2-external'
	@echo
	@echo 'These targets do not invoke docker when called outside it:'
	@echo '   submodules                   = update submodules'
	@echo ' * reconfig                     = force update from saved defconfig to .config in the build directories'
	@echo ' * rebuild                      = regenerate target/ by reinstalling host packages and rebuilding target packages'
	@echo ' * image                        = ensure buildroot will recheck if any step in the dependency chain needs something to be built'
	@echo ' * clean                        = remove all files generated in the build'
	@echo '   distclean                    = "clean" + clear all caches (download and ccache)'
	@echo '   help                         = show this help'
	@echo
	@echo '* = this target can be called as <defconfig>+<target>, e.g. rpi3+os to apply to only one defconfig'

ifeq ($(check_inside_docker),n) ########################################

$(real_targets_inside_docker) $(phony_targets_inside_docker): .stamp_submodules
	@echo "====== $@ ======"
	@utils/docker-run $(MAKE) $@

else # ($(check_inside_docker),n) ########################################

config: .stamp_config
	@echo "=== $@ ==="
.stamp_config: .stamp_submodules $(foreach defconfig,$(configs_to_build),.stamp_config_$(defconfig))
	@echo "=== $@ ==="
	@touch $@
$(foreach defconfig,$(configs_to_build),$(defconfig)+config): %+config: .stamp_config_%
$(foreach defconfig,$(configs_to_build),.stamp_config_$(defconfig)): .stamp_submodules
	@echo "--- $@ ---"
	@mkdir -p $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_config_%,%,$@)
	defconfig_base_path=$(BR2_EXTERNAL2_DIR)/$(shell grep ^BR2_DEFCONFIG_BASE= $(BUILD_SRC_DIR)/$(patsubst .stamp_config_%,%,$@)$(DEFCONFIG_SUFFIX) | sed -e 's,^.*=,,g' -e 's,",,g') && \
		defconfig_base_name="$$(basename $${defconfig_base_path})" && \
		defconfig_overlay=$(BR2_EXTERNAL2_DIR)/$(shell grep ^BR2_DEFCONFIG_OVERLAY= $(BUILD_SRC_DIR)/$(patsubst .stamp_config_%,%,$@)$(DEFCONFIG_SUFFIX) | sed -e 's,^.*=,,g' -e 's,",,g') && \
		$(MAKE) -C $(BUILDROOT_DIR) BR2_EXTERNAL='$(BR2_EXTERNAL1_DIR) $(BR2_EXTERNAL2_DIR)' O=$(BUILD_OUTPUT_DIR)/$(patsubst .stamp_config_%,%,$@) $${defconfig_base_name} 2>&1 | \
			tee -a $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_config_%,%,$@)/br.log && \
		$(MERGE_CONFIG_SCRIPT) -m -O $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_config_%,%,$@) $${defconfig_base_path} $${defconfig_overlay} $(BUILD_SRC_DIR)/$(patsubst .stamp_config_%,%,$@)$(DEFCONFIG_SUFFIX) 2>&1 | \
			tee -a $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_config_%,%,$@)/br.log
	@cd $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_config_%,%,$@) && \
		$(BRMAKE_SCRIPT) olddefconfig
	@touch $@

source: .stamp_source
	@echo "=== $@ ==="
.stamp_source: .stamp_config $(foreach defconfig,$(configs_to_build),.stamp_source_$(defconfig))
	@echo "=== $@ ==="
	@touch $@
$(foreach defconfig,$(configs_to_build),$(defconfig)+source): %+source: .stamp_source_%
$(foreach defconfig,$(configs_to_build),.stamp_source_$(defconfig)): .stamp_source_%: .stamp_config_%
	@echo "--- $@ ---"
	@cd $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_source_%,%,$@) && \
		$(BRMAKE_SCRIPT) source
	@touch $@

toolchain: .stamp_toolchain
	@echo "=== $@ ==="
.stamp_toolchain: .stamp_source $(foreach defconfig,$(configs_to_build),.stamp_toolchain_$(defconfig))
	@echo "=== $@ ==="
	@touch $@
$(foreach defconfig,$(configs_to_build),$(defconfig)+toolchain): %+toolchain: .stamp_toolchain_%
$(foreach defconfig,$(configs_to_build),.stamp_toolchain_$(defconfig)): .stamp_toolchain_%: .stamp_source_%
	@echo "--- $@ ---"
	@cd $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_toolchain_%,%,$@) && \
		$(BRMAKE_SCRIPT) toolchain
	@touch $@

os-depends: .stamp_os_depends
	@echo "=== $@ ==="
.stamp_os_depends: .stamp_toolchain $(foreach defconfig,$(configs_to_build),.stamp_os_depends_$(defconfig))
	@echo "=== $@ ==="
	@touch $@
$(foreach defconfig,$(configs_to_build),$(defconfig)+os-depends): %+os-depends: .stamp_os_depends_%
$(foreach defconfig,$(configs_to_build),.stamp_os_depends_$(defconfig)): .stamp_os_depends_%: .stamp_toolchain_%
	@if grep -q 'BR2_TARGET_UBOOT=y' $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_os_depends_%,%,$@)/.config; then \
		echo "--- (uboot) $@ ---" ; \
		cd $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_os_depends_%,%,$@) && \
			$(BRMAKE_SCRIPT) uboot-depends ; \
	else \
		echo "--- (skip uboot) $@ ---" ; \
	fi
	@if grep -q 'BR2_LINUX_KERNEL=y' $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_os_depends_%,%,$@)/.config; then \
		echo "--- (linux) $@ ---" ; \
		cd $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_os_depends_%,%,$@) && \
			$(BRMAKE_SCRIPT) linux-depends ; \
	else \
		echo "--- (skip linux) $@ ---" ; \
	fi
	@touch $@

os: .stamp_os
	@echo "=== $@ ==="
.stamp_os: .stamp_os_depends $(foreach defconfig,$(configs_to_build),.stamp_os_$(defconfig))
	@echo "=== $@ ==="
	@touch $@
$(foreach defconfig,$(configs_to_build),$(defconfig)+os): %+os: .stamp_os_%
$(foreach defconfig,$(configs_to_build),.stamp_os_$(defconfig)): .stamp_os_%: .stamp_os_depends_%
	@if grep -q 'BR2_TARGET_UBOOT=y' $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_os_%,%,$@)/.config; then \
		echo "--- (uboot) $@ ---" ; \
		cd $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_os_%,%,$@) && \
			$(BRMAKE_SCRIPT) uboot ; \
	else \
		echo "--- (skip uboot) $@ ---" ; \
	fi
	@if grep -q 'BR2_LINUX_KERNEL=y' $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_os_%,%,$@)/.config; then \
		echo "--- (linux) $@ ---" ; \
		cd $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_os_%,%,$@) && \
			$(BRMAKE_SCRIPT) linux ; \
	else \
		echo "--- (skip linux) $@ ---" ; \
	fi
	@touch $@

init: .stamp_init
	@echo "=== $@ ==="
.stamp_init: .stamp_os $(foreach defconfig,$(configs_to_build),.stamp_init_$(defconfig))
	@echo "=== $@ ==="
	@touch $@
$(foreach defconfig,$(configs_to_build),$(defconfig)+init): %+init: .stamp_init_%
$(foreach defconfig,$(configs_to_build),.stamp_init_$(defconfig)): .stamp_init_%: .stamp_os_%
	@if grep -q 'BR2_PACKAGE_SYSTEMD=y' $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_init_%,%,$@)/.config; then \
		echo "--- (systemd) $@ ---" ; \
		cd $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_init_%,%,$@) && \
			$(BRMAKE_SCRIPT) systemd ; \
	else \
		echo "--- (skip systemd) $@ ---" ; \
	fi
	@touch $@

updater: .stamp_updater
	@echo "=== $@ ==="
.stamp_updater: .stamp_init $(foreach defconfig,$(configs_to_build),.stamp_updater_$(defconfig))
	@echo "=== $@ ==="
	@touch $@
$(foreach defconfig,$(configs_to_build),$(defconfig)+updater): %+updater: .stamp_updater_%
$(foreach defconfig,$(configs_to_build),.stamp_updater_$(defconfig)): .stamp_updater_%: .stamp_init_%
	@if grep -q 'BR2_PACKAGE_SWUPDATE=y' $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_updater_%,%,$@)/.config; then \
		echo "--- (swupdate) $@ ---" ; \
		cd $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_updater_%,%,$@) && \
			$(BRMAKE_SCRIPT) swupdate ; \
	else \
		echo "--- (skip swupdate) $@ ---" ; \
	fi
	@touch $@

all: .stamp_all
	@echo "=== $@ ==="
.stamp_all: .stamp_updater $(foreach defconfig,$(configs_to_build),.stamp_all_$(defconfig))
	@echo "=== $@ ==="
	@touch $@
$(foreach defconfig,$(configs_to_build),$(defconfig)+all): %+all: .stamp_all_%
$(foreach defconfig,$(configs_to_build),.stamp_all_$(defconfig)): .stamp_all_%: .stamp_updater_%
	@echo "--- $@ ---"
	@cd $(BUILD_OUTPUT_DIR)/$(patsubst .stamp_all_%,%,$@) && \
		$(BRMAKE_SCRIPT) all
	@touch $@

menuconfig: .stamp_config $(foreach defconfig,$(configs_to_build),$(defconfig)+menuconfig)
	@echo "=== $@ ==="
$(foreach defconfig,$(configs_to_build),$(defconfig)+menuconfig): %+menuconfig: .stamp_config_%
	@echo "--- $@ ---"
	@make -C $(BUILD_OUTPUT_DIR)/$(patsubst %+menuconfig,%,$@) menuconfig
	@make -C $(BUILD_OUTPUT_DIR)/$(patsubst %+menuconfig,%,$@) savedefconfig
	@rm -rf $(BUILD_OUTPUT_DIR)/$(patsubst %+menuconfig,%,$@)/merge
	@mkdir -p $(BUILD_OUTPUT_DIR)/$(patsubst %+menuconfig,%,$@)/merge
	defconfig_base_path=$(BR2_EXTERNAL2_DIR)/$(shell grep ^BR2_DEFCONFIG_BASE= $(BUILD_SRC_DIR)/$(patsubst %+menuconfig,%,$@)$(DEFCONFIG_SUFFIX) | sed -e 's,^.*=,,g' -e 's,",,g') && \
		defconfig_base_name="$$(basename $${defconfig_base_path})" && \
		$(MAKE) -C $(BUILDROOT_DIR) BR2_EXTERNAL='$(BR2_EXTERNAL1_DIR) $(BR2_EXTERNAL2_DIR)' O=$(BUILD_OUTPUT_DIR)/$(patsubst %+menuconfig,%,$@)/merge $${defconfig_base_name} 2>&1 | \
			tee -a $(BUILD_OUTPUT_DIR)/$(patsubst %+menuconfig,%,$@)/br.log && \
		$(MERGE_CONFIG_SCRIPT) -m -O $(BUILD_OUTPUT_DIR)/$(patsubst %+menuconfig,%,$@)/merge $(BUILD_OUTPUT_DIR)/$(patsubst %+menuconfig,%,$@)/defconfig $${defconfig_base_path} 2>&1 | \
			tee -a $(BUILD_OUTPUT_DIR)/$(patsubst %+menuconfig,%,$@)/br.log && \
		cd $(BUILD_OUTPUT_DIR)/$(patsubst %+menuconfig,%,$@) && \
			$(BRMAKE_SCRIPT) -C merge olddefconfig savedefconfig && \
		$(DIFF_CONFIG_SCRIPT) -m $${defconfig_base_path} $(BUILD_OUTPUT_DIR)/$(patsubst %+menuconfig,%,$@)/merge/defconfig | \
			tee $(BUILD_SRC_DIR)/$(patsubst %+menuconfig,%,$@)$(DEFCONFIG_SUFFIX)

legal-info: .stamp_toolchain $(foreach defconfig,$(configs_to_build),$(defconfig)+legal-info)
	@echo "=== $@ ==="
$(foreach defconfig,$(configs_to_build),$(defconfig)+legal-info): %+legal-info: .stamp_toolchain_%
	@echo "--- $@ ---"
	@make -C $(BUILD_OUTPUT_DIR)/$(patsubst %+legal-info,%,$@) legal-info

check-package:
	@echo "=== $@ ==="
	@git ls-files | xargs rpi3/buildroot/utils/check-package -b

endif # ($(check_inside_docker),n) ########################################

$(foreach defconfig,$(configs_to_build),$(defconfig)+): %+: %+all
	@echo "+++ $@ +++"

submodules: .stamp_submodules
	@echo "=== $@ ==="
.stamp_submodules:
	@echo "=== $@ ==="
	@git submodule init
	@git submodule update --recursive
	@touch $@

clean-stamps: $(foreach defconfig,$(configs_to_build),$(defconfig)+clean-stamps)
	@echo "=== $@ ==="
	@rm -rf .stamp_submodules
	@rm -rf .stamp_*
$(foreach defconfig,$(configs_to_build),$(defconfig)+clean-stamps): %+clean-stamps:
	@echo "--- $@ ---"
	@rm -rf .stamp_*_$(patsubst %+clean-stamps,%,$@)

$(foreach defconfig,$(configs_to_build),$(defconfig)+clean-target): %+clean-target: %+clean-target-workaround-failed-reinstall
$(foreach defconfig,$(configs_to_build),$(defconfig)+clean-target-workaround-failed-reinstall): %+clean-target-workaround-failed-reinstall:
	@echo "--- $@ ---"
	@rm -rf $(BUILD_OUTPUT_DIR)/$(patsubst %+clean-target-workaround-failed-reinstall,%,$@)/build/host-libopenssl-*/
	@rm -rf $(BUILD_OUTPUT_DIR)/$(patsubst %+clean-target-workaround-failed-reinstall,%,$@)/build/host-python3-*/

clean-target: $(foreach defconfig,$(configs_to_build),$(defconfig)+clean-target)
	@echo "=== $@ ==="
$(foreach defconfig,$(configs_to_build),$(defconfig)+clean-target): %+clean-target: %+clean-stamps
	@echo "--- $@ ---"
	@rm -rf $(BUILD_OUTPUT_DIR)/$(patsubst %+clean-target,%,$@)/build/*/.stamp*_installed
	@rm -rf $(BUILD_OUTPUT_DIR)/$(patsubst %+clean-target,%,$@)/images/*
	@rm -rf $(BUILD_OUTPUT_DIR)/$(patsubst %+clean-target,%,$@)/target/*

reconfig: clean-stamps config
	@echo "=== $@ ==="
$(foreach defconfig,$(configs_to_build),$(defconfig)+reconfig): %+reconfig: %+clean-stamps %+config
	@echo "--- $@ ---"

rebuild: clean-target all
	@echo "=== $@ ==="
$(foreach defconfig,$(configs_to_build),$(defconfig)+rebuild): %+rebuild: %+clean-target %+all
	@echo "--- $@ ---"

image: clean-stamps all
	@echo "=== $@ ==="
$(foreach defconfig,$(configs_to_build),$(defconfig)+image): %+image: %+clean-stamps %+all
	@echo "--- $@ ---"

clean: clean-stamps $(foreach defconfig,$(configs_to_build),$(defconfig)+clean)
	@echo "=== $@ ==="
$(foreach defconfig,$(configs_to_build),$(defconfig)+clean): %+clean: %+clean-stamps
	@echo "--- $@ ---"
	@rm -rf $(BUILD_OUTPUT_DIR)/$(patsubst %+clean,%,$@)/

distclean: clean
	@echo "=== $@ ==="
	@rm -rf $(DOWNLOAD_CACHE_DIR)
	@rm -rf $(BUILDROOT_DIR)
	@rm -rf $(BR2_EXTERNAL1_DIR)
	@rm -rf $(CCACHE_DIR)
